AndroidEclipseTweak
---

An Android Eclipse Tweak to make it look a stellar Android IDE. This if for those 
who use Linux and thus the gtkrc is tied to the theme engine one might be using 
OS wise. If not using Linux than only use the css file and rename it to your OS
type and avoid using the bash script and gtkrc files.

# What is Included

A GTKRC file
A bash script file
An css file

# Requirements

Requires Eclipse 4.x or higher.

# Dependencies

On Linux I use claerlooks theme engine and abwatia so the gtkrc file is tied to that theme 
engine. However, you can use it as a guide to change your theme engine gtkrc file
to match mine and it should work.

# Installing

## Linux

Install the bash script and the css file in your eclipse folder. Its the GTK 
choice in windows general appearance.

## Mac

Rename the css file to the one the Eclipse platform expects for Mac.
Install the css file in your metadata hidden folder named .e4css that you create 
in your user home directory. Its the Mac choice in windows general appearance.

## Windows

Rename the css file to the Windows OS variation name that Eclipse expects.
Install the css file matchingyour system OS version in the metadate hidden folder 
you create in your user home directory named .e4css. It will be the Windows 
variation choice name in windows general appearance.

# Project Status

Currently usable but beta.

# Project License

My modifications are under Copyright 2012, Apache License 2.0.
